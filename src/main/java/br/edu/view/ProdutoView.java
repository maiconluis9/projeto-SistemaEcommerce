package br.edu.view;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.dao.ProdutoBD;

public class ProdutoView extends JFrame {
	   
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ProdutoBD pro = new ProdutoBD();
	
	
		private JPanel cadastroPanel;
	    private JTextField campoNomeProduto;
		private JTextField campoQuantidade;
		private JTextField campoPreco;	
		private JTextField campoId;
		private JLabel id;
		private JLabel produto;
		private JLabel qntProduto;
		private JLabel preco;		
		private JButton inserirButton;
		private JButton deletButton;
		private JButton atualizaButton;
		private JButton listaButton;
		private JButton cancelarButton;
		
		
		public ProdutoView(String string) {
			super("Cadastrar Produtos:");
			campoNomeProduto = new JTextField(25);
			campoQuantidade = new JTextField(8);
			campoPreco = new JTextField(5);
			campoId = new JTextField(5);
			id = new JLabel("Codigo: ");
			produto = new JLabel("Produto:");
			qntProduto = new JLabel("Quantidade:");
			preco = new JLabel("Pre�o:");
			
			
			
			inserirButton = new JButton("Inserir");			
			deletButton = new JButton("Deletar");
			atualizaButton = new JButton("Atualizar");
			listaButton = new JButton("Selecionar");
			cancelarButton = new JButton("Cancelar");

			
			
		
		}
		
		public void mostrarTela() {
			//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLocation(250, 300);			
			setLocationRelativeTo(null);
			this.setResizable(false);
			cadastroPanel = new JPanel();
			cadastroPanel.setLayout(new GridLayout(4,1,0,10));				
			setLayout(new GridLayout(6, 1));
			Container cp = getContentPane();
			JPanel [] cadastroPanel = new JPanel[6]; //vetor de JPanel
			for (int i = 0 ; i < cadastroPanel.length; i++) {
				cadastroPanel[i] = new JPanel(); //inicializa cada JPanel
			}
			cadastroPanel[0].add(id);
			cadastroPanel[0].add(campoId);
			cadastroPanel[1].add(produto);
			cadastroPanel[1].add(campoNomeProduto);
			cadastroPanel[2].add(qntProduto);
			cadastroPanel[2].add(campoQuantidade);
			cadastroPanel[2].add(preco);
			cadastroPanel[2].add(campoPreco);
			cadastroPanel[3].add(inserirButton);
			cadastroPanel[3].add(deletButton);
			cadastroPanel[3].add(atualizaButton);
			cadastroPanel[0].add(listaButton);
			cadastroPanel[3].add(cancelarButton);
					
							
							
			for (int i = 0; i < cadastroPanel.length; i++) {
				cp.add(cadastroPanel[i]);//adiciona cada linha ao JFrame
			}
			pack();
			setVisible(true);
			
			inserirButton.addActionListener(new ActionListener() {	
				
				public void actionPerformed(ActionEvent arg0) {
				
					try {					
						pro.inserir(campoId.getText(),campoNomeProduto.getText(), campoQuantidade.getText(), campoPreco.getText());					
						JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "ERRO DE EXECU��O!!"+e.getMessage());
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
					}
				}
				
			});
			
			listaButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {				
					campoNomeProduto.setText("");
					try {
						campoNomeProduto.setText(pro.listar(campoNomeProduto.getText()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					campoId.setText("");
					try {
						campoId.setText(pro.listar(campoId.getText()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					campoPreco.setText("");
					try {
						campoPreco.setText(pro.listar(campoPreco.getText()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					campoQuantidade.setText("");
					try {
						campoQuantidade.setText(pro.listar(campoQuantidade.getText()));
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			});	
			
			deletButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					try {
						pro.apagar(campoId.getText());
						campoId.setText("");
						campoNomeProduto.setText("");
						campoQuantidade.setText("");
						campoPreco.setText("");
						JOptionPane.showMessageDialog(null,"Registro exclu�do com sucesso!!");
					} catch (SQLException e) {
						JOptionPane.showMessageDialog(null, "ERRO NA EXECU��O!!"+e.getMessage());
			
					}
				}	
			});
			
			atualizaButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						pro.atualizar(campoId.getText(), campoNomeProduto.getText(), campoPreco.getText(), campoQuantidade.getText());
						JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
					}catch (SQLException e) {
						JOptionPane.showMessageDialog(null, "Cliente n�o disponivel!!"+e.getMessage());
					}
							
				}
			});
			
			
			
			
		cancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProdutoView.this.dispose();
			}
		});
		
		}
}

	


		
	


