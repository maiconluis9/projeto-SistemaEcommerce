package br.edu.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import br.edu.dao.Conexao;
import br.edu.dao.ProdutoBD;
import br.edu.model.Produto;

public class CompraView extends JFrame {
	/**
	 * @author Maicon Luis
	 */
	private static final long serialVersionUID = 1L;
	ProdutoBD pro = new ProdutoBD();

	JLabel titulo = new JLabel("**** PEDIDO ****");
    JLabel clie = new JLabel("Cliente");
    JTextField cod_clie = new JTextField();
    JLabel prod = new JLabel("Produto:");
    JComboBox<Produto> produtoC = new JComboBox<Produto>();
    JLabel cod_p = new JLabel("2");    
    JLabel valor = new JLabel("Valor:");
    JTextField pvalor = new JTextField("");
    JButton enviar = new JButton("Confirmar");
    JButton cancelar = new JButton("Cancelar");

	
    
    public CompraView() {

        this.setTitle("COMPRAS");
        this.setSize(450, 230);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setVisible(true);
        JPanel p = new JPanel(null);

        titulo.setBounds(170, 10, 150, 20);
        clie.setBounds(10, 40, 80, 20);
        cod_clie.setBounds(100, 40, 40, 20);
        prod.setBounds(150, 40, 100, 20);
        produtoC.setBounds(230, 40, 150, 20);
        cod_p.setBounds(390, 40, 50, 20);
        valor.setBounds(150, 70, 50, 20);
        pvalor.setBounds(230, 70, 50, 20);
        enviar.setBounds(90, 130, 100, 30);
        cancelar.setBounds(250, 130, 100, 30);

        p.add(titulo);
        p.add(clie);
        p.add(cod_clie);
        p.add(prod);
        p.add(produtoC);
        p.add(cod_p);
        p.add(valor);
        p.add(pvalor);

        p.add(enviar);
        p.add(cancelar);

        add(p);
    
        produtoC.addActionListener(new ActionListener() {
          
            public void actionPerformed(ActionEvent e) {          
              try {
                    String sql = "SELECT descricao FROM produto"
                            + "WHERE idProduto = " + cod_clie.getText();
                    Connection conn = Conexao.getConexao();
            		PreparedStatement stmt = conn.prepareStatement(sql);
            		ResultSet rs = stmt.executeQuery();	
            		rs.next();
                 
                } catch (SQLException ex) {
                    System.out.println("Erro SQL: " + ex);
                }
            }
        });
        
        
              
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CompraView.this.dispose();
			}
		});
}
}