package br.edu.view;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.dao.CompraBD;

public class CompraViiew extends JFrame{
	
	 private CompraBD comp = new CompraBD();
	 /**
	 *@author Maicon Luis
	 */
	private static final long serialVersionUID = 1L;
	private JPanel cadastroPanel;
	private JTextField campoId;
    private JTextField campoQuantidade;
	private JTextField campoIdProduto;
	private JLabel id;
	private JLabel quantidade;
	private JLabel idProduto;
	private JButton inserirButton;
	private JButton deletButton;
	private JButton atualizaButton;
	private JButton listaButton;
	private JButton cancelarButton;	
	
	public CompraViiew() {
		super("Cadastrar Compra:");
		campoId = new JTextField(5);
		campoQuantidade = new JTextField(5);
		campoIdProduto = new JTextField(5);
		id = new JLabel("Codigo Compra: ");
		quantidade = new JLabel("Quantidade:");
		idProduto = new JLabel("Codigo do Produto:");
		
		inserirButton = new JButton("Validar");			
		deletButton = new JButton("Deletar");
		atualizaButton = new JButton("Atualizar");
		listaButton = new JButton("Pesquisar");
		cancelarButton = new JButton("Cancelar");			
	
	
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(150, 300);			
		setLocationRelativeTo(null);
		this.setResizable(false);
		cadastroPanel = new JPanel();
		cadastroPanel.setLayout(new GridLayout(4,1));			
		setLayout(new GridLayout(6,1));
		Container cp = getContentPane();
		JPanel [] cadastroPanel = new JPanel[6]; //vetor de JPanel
		for (int i = 0 ; i < cadastroPanel.length; i++) {
			cadastroPanel[i] = new JPanel(); //inicializa cada JPanel
		}
		cadastroPanel[0].add(id);
		cadastroPanel[0].add(campoId);
		cadastroPanel[1].add(idProduto);
		cadastroPanel[1].add(campoIdProduto);
		cadastroPanel[1].add(quantidade);
		cadastroPanel[1].add(campoQuantidade);
		
		
		cadastroPanel[2].add(inserirButton);
		cadastroPanel[2].add(deletButton);
		cadastroPanel[2].add(atualizaButton);
		cadastroPanel[0].add(listaButton);
		cadastroPanel[2].add(cancelarButton);
		
			
		for (int i = 0; i < cadastroPanel.length; i++) {
			cp.add(cadastroPanel[i]);//adiciona cada linha ao JFrame
		}
		pack();
		setVisible(true);		
		
		
			inserirButton.addActionListener(new ActionListener() {	
				public void actionPerformed(ActionEvent arg0) {
				 try {					
					comp.inserir(campoId.getText(),campoQuantidade.getText());					
					JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Cliente n�o disponivel!!"+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});


}
}
