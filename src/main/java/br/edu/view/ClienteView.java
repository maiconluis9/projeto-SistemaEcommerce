package br.edu.view;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.dao.ClienteBD;

public class ClienteView extends JFrame {
		
	    /**
	    * @author Maicon Luis  
	    */
	private static final long serialVersionUID = -8033244406261518561L;
	
	
		private JPanel cadastroPanel;
		private JTextField campoId;
	    private JTextField campoNome;
		private JTextField campoTelefone;
		private JTextField campoEndereco;		
		private JTextField campoCPF;
		private JTextField campoNascimento;
		private JLabel id;
		private JLabel nome;		
		private JButton inserirButton;
		private JButton deletButton;
		private JButton atualizaButton;
		private JButton listaButton;
		private JButton cancelarButton;		
		
		private ClienteBD cli = new ClienteBD();
		
		public ClienteView(String string) {
			super("Cadastrar Cliente:");
			campoId = new JTextField(5);
			campoNome = new JTextField(45);
			campoTelefone = new JTextField(8);
			campoEndereco = new JTextField(45);
			campoCPF = new JTextField(10);
			campoNascimento = new JTextField(10);
			id = new JLabel("Codigo: ");
			nome = new JLabel("Nome:");
			inserirButton = new JButton("Inserir");			
			deletButton = new JButton("Deletar");
			atualizaButton = new JButton("Atualizar");
			listaButton = new JButton("Pesquisar");
			cancelarButton = new JButton("Cancelar");			
		
		}
		
		public void mostrarTela() {
			//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLocation(150, 300);			
			setLocationRelativeTo(null);
			this.setResizable(false);
			cadastroPanel = new JPanel();
			cadastroPanel.setLayout(new GridLayout(4,1));			
			setLayout(new GridLayout(6,1));
			Container cp = getContentPane();
			JPanel [] cadastroPanel = new JPanel[6]; //vetor de JPanel
			for (int i = 0 ; i < cadastroPanel.length; i++) {
				cadastroPanel[i] = new JPanel(); //inicializa cada JPanel
			}
		
			cadastroPanel[0].add(id);
			cadastroPanel[0].add(campoId);
			cadastroPanel[1].add(nome);
			cadastroPanel[1].add(campoNome);
			
			cadastroPanel[2].add(inserirButton);
			cadastroPanel[2].add(deletButton);
			cadastroPanel[2].add(atualizaButton);
			cadastroPanel[0].add(listaButton);
			cadastroPanel[2].add(cancelarButton);
			
				
			for (int i = 0; i < cadastroPanel.length; i++) {
				cp.add(cadastroPanel[i]);//adiciona cada linha ao JFrame
			}
			pack();
			setVisible(true);		
			
			
		inserirButton.addActionListener(new ActionListener() {	
			
			public void actionPerformed(ActionEvent arg0) {
			
				try {					
					cli.inserir(campoId.getText(),campoNome.getText(), campoTelefone.getText(), campoEndereco.getText(), campoCPF.getText(), campoNascimento.getText());					
					JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Cliente n�o disponivel!!"+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		listaButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {				
				campoNome.setText("");
				campoNome.setText(cli.listar(campoId.getText()));				
			}
			
		});	
		
		atualizaButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					cli.atualizar(campoId.getText(),campoNome.getText());
					JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");
				}catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Cliente n�o disponivel!!"+e.getMessage());
				}
			}
			
		});
		
		deletButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					cli.apagar(campoId.getText());
					campoId.setText("");
					nome.setText("");
					JOptionPane.showMessageDialog(null,"Registro exclu�do com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "ERRO NA EXECU��O!!"+e.getMessage());
		
				}
			}	
		});
		
		
		cancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClienteView.this.dispose();
			}
		});
		
		}
	}














