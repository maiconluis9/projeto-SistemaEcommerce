package br.edu.view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuView extends JFrame {			
	
	private static final long serialVersionUID = 1L;

		public MenuView() {
		super("Controle de Ecommerce"); 
		criarMenu();
		
		setLocationRelativeTo(null); // deixa a janela no centro
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
		this.setResizable(false);
		setSize(330, 160);
		setVisible(true);
		setLocationRelativeTo(null); // comando para aparecer a janela centralizada
		// setResizable(false); bloquea a renderização da tela
		
	}
	
	private void criarMenu() {
		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);	
		
		
		JMenu menuCadastro = new JMenu("Cadastro");
		JMenuItem menuCliente = new JMenuItem("Cliente");		
		menuCadastro.add(menuCliente);	
	    JMenuItem menuProduto = new JMenuItem("Produto");
		menuCadastro.add(menuProduto);
		
		JMenu menuCarrinho = new JMenu("Compra");
		JMenuItem menuCompra = new JMenuItem("Adicionar");
		menuCarrinho.add(menuCompra);
		JMenuItem menuConsulta = new JMenuItem("Consulta");
		menuCarrinho.add(menuConsulta);
		
		menuCliente.addActionListener(new ActionListener() {
			
			
			
			
			

		public void actionPerformed(ActionEvent e) {
			ClienteView executar = new ClienteView("Cadastro Cliente: ");				
			executar.mostrarTela();
			}

		});	
			
		menuCompra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CompraView com = new CompraView();
				
			}			
		});
			
		
		
		
		menuProduto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProdutoView exe = new ProdutoView("Cadastro Produto: ");
				exe.mostrarTela();
			}
			
		});
		
		barra.add(menuCadastro);		
		barra.add(menuCarrinho);		
	}
	
		

}
