package br.edu.model;

import java.util.ArrayList;
import java.util.List;

public class Compra extends Cliente{

	public Compra(int idCliente, String nome, List<Compra> compras) {
		super(idCliente, nome, compras);
		// TODO Auto-generated constructor stub
	}

	private int id;
	private String codigo;
	private int idCliente;
	private int idProduto;
	private int quantidade;		
	
	private List<ItemCompra> itens = new ArrayList<ItemCompra>();	

		
	public List<ItemCompra> getItens(){
		return itens;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
}
