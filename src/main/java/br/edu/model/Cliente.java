package br.edu.model;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	
	private int idCliente;
	private String nome;
	
	private List<Compra> compras = new ArrayList<Compra>();
	
	public void adicionarCompra(Compra compra){
		compras.add(compra);
	}
	
	public List<Compra> getCompras(){
		return compras;
	}
	
	public void removerCompraPorNumero(int numero) {
		for(Compra compra:compras) {
			if(compra.getIdProduto() == numero) {
				compras.remove(numero);
			}
		}
	}
	
	public Compra pegarCompraPorNumero(int numero) {
		for(Compra compra: compras) {
			if(compra.getIdProduto() == numero) {
				return compra;
			}
		}
		return null;
	}

	
	public Cliente(int idCliente, String nome, List<Compra> compras) {
		this.idCliente = idCliente;
		this.nome = nome;
		this.compras = compras;
	}


	public int getIdCliente() {
		return idCliente;
		
	}
	
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	public String getNome() {
		return nome;
		
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	

}
