package br.edu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.swing.JComboBox;

import br.edu.model.Compra;
import br.edu.model.Produto;

public class ProdutoComb extends Conexao{
	Produto produto = new Produto();
	
	public ProdutoComb() {
	}  
	
	
	
	public Vector<Produto> listarProduto () throws Exception {
		String url = "";
		
		Vector<Produto> listProduto = new Vector<Produto>();
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("SELECT descricao FROM produto");
		ResultSet rs = stmt.executeQuery();	
		
		while(rs.next()) {
			
			Produto obj = new Produto();
			obj.setId(rs.getInt("idProduto"));
			obj.setNomeProduto(rs.getString("descricao"));
			listProduto.add(obj);
			
			
		}
		return listProduto;
	}
	

 }
