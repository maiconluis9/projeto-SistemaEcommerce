package br.edu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ProdutoBD  {
	

	
	public void inserir(String idProduto,String descricao, String preco, String quantidade) throws Exception {		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into produto values(?,?,?,?)");	
		stmt.setInt(1, Integer.parseInt(idProduto));
		stmt.setString(2, descricao);
		stmt.setDouble(3,Double.parseDouble(preco));
		stmt.setDouble(4,Double.parseDouble(quantidade));
		if (descricao.equals("")) {
			throw new Exception("Nome n�o pode ser vazio!!");		
		} else {
			stmt.executeUpdate();
		}
		stmt.close();
		conn.close();		
	}
	
	public void apagar(String campoId) throws SQLException {		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update produto set descricao=? where idProduto=?");				
		stmt.setInt(1, Integer.parseInt(campoId));
		stmt.executeUpdate();
		stmt.close();
		conn.close();

	}
	public void atualizar(String idProduto, String descricao,String preco, String quantidade) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("UPDATE produto set descricao=?,preco=?,quantidade=? where idProduto=?");		
		stmt.setString(1, descricao);			
		stmt.setDouble(2,Double.parseDouble(preco));
		stmt.setInt(3,Integer.parseInt(quantidade));
		stmt.setInt(4, Integer.parseInt(idProduto));
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}

	
	public List<String> listarTodos(){
		List<String> Produto = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from produto");
			while (rs.next()) {
				Produto.add(rs.getString("campoNomeProduto"));
				Produto.add(rs.getString("campoQuantidade"));
				Produto.add(rs.getString("campoPreco"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Produto;
	}
	
	public String listar(String idProduto) throws SQLException {
		String sql="";	
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("SELECT  descricao, preco, quantidade FROM produto WHERE idProduto=? ");	
			stmt.setInt(1, Integer.parseInt(idProduto));
			ResultSet rs = stmt.executeQuery();		
			if (rs.next()) {
				sql = rs.getString(1);
				sql = rs.getString(2);
				sql = rs.getString(3);
				sql = rs.getString(4);		
			}			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return sql;				
	}
	
	/*
	public Vector<ProdutoBD> listarProduto () throws SQLException {
		Vector<ProdutoBD> listar = new Vector<ProdutoBD>();
		String sql = "Select descricao From produto";
		Connection c = Conexao.getConexao();
		PreparedStatement ps = c.prepareStatement(sql);	
		ResultSet rs = ps.executeQuery();	
		while (rs.next()) {			
			ProdutoBD nome = new ProdutoBD();
			nome.getClass(rs.getString("descricao"));
			listar.add(descricao);
		}
		ps.close();
		c.close();
		return listar;
	}
	
	*/
	
	
	
	public String listaNomeProduto(String descricao)throws SQLException {
		String nome="";
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("SELECT descricao FROM produto");
		stmt.setString(1, (descricao));
		ResultSet rs = stmt.executeQuery();	
		if(rs.next()) {
			nome = rs.getString(1);
		}
		if(nome.equals("")) {
			try {
				throw new Exception("O nome do cliente est� em branco!!");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stmt.close();
			conn.close();

		}
		return nome;
	}
}
