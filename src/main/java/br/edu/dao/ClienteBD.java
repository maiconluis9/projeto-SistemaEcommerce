package br.edu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClienteBD {
	
	
	public void inserir(String idClientes,String nome, String telefone, String CPF, String endereco, String dataNascimento) throws Exception {		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into clientes values(?,?,?,?,?,?)");	
		stmt.setInt(1, Integer.parseInt(idClientes));
		stmt.setString(2, nome);
		stmt.setString(3, telefone);
		stmt.setString(4, CPF);
		stmt.setString(5, endereco);
		stmt.setString(6, dataNascimento);
		if (nome.equals("")) {
			throw new Exception("Nome n�o pode ser vazio!!");		
		} else {
			stmt.executeUpdate();
		}
		stmt.close();
		conn.close();		
	}
	
	public void apagar(String campoId) throws SQLException {		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update clientes set nome=? where idClientes=?");				
		stmt.setInt(1, Integer.parseInt(campoId));
		stmt.executeUpdate();
		stmt.close();
		conn.close();

	}
	public void atualizar(String idClientes, String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("UPDATE clientes set nome=? where idClientes=?");		
		stmt.setString(1, nome);	
		stmt.setInt(2, Integer.parseInt(idClientes));
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}

	
	public List<String> listarTodos(){
		List<String> nomeClientes = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();;
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from clientes");
			while (rs.next()) {
				nomeClientes.add(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomeClientes;
	}
	
	public String listar(String idClientes) {
		String sql="";		
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from clientes where idClientes=?");	
			stmt.setInt(1, Integer.parseInt(idClientes));
			ResultSet rs = stmt.executeQuery();						
			if (rs.next()) {
				sql = rs.getString(1);				
			}
			if (sql.equals("")){
				throw new Exception("O nome do cliente est� em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return sql;
		
		
	}

	


}
