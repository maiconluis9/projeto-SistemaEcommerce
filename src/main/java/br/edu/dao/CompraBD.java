package br.edu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class CompraBD {
	
	public void inserir(String idItem,String  quantidade) throws Exception {		
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into produto values(?,?)");	
		stmt.setInt(1, Integer.parseInt(idItem));
		stmt.setInt(2, Integer.parseInt(quantidade));
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}


}
