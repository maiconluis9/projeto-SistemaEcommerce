package br.edu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexao {
	
	private static Connection connection = null;
	private static Statement statement = null; 
	private ResultSet resultset = null;
	
	public static Connection  getConexao() {
		String usuario = "root";
		String senha = "";
		String driver = "com.mysql.cj.jdbc.Driver";
		String servidor = "jdbc:mysql://127.0.0.1:3306/sistemaprogramacao?useTimezone=true&serverTimezone=UTC";
		try {
				Class.forName(driver);
				connection = DriverManager.getConnection(servidor, usuario, senha);
				statement = connection.createStatement();
		} catch (Exception e) {
			System.out.println("ERRO" + e.getMessage());
		}
		return connection;		
	}
	public boolean estaConectado() {
		if(this.connection != null) {
			return true;
		} else {
			return false;
		}	
	}
	
	
	

}
